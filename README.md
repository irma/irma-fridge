# irma-fridge

This repository contains two application: `irma-fridge server` and  `irma-fridge client`.
The code is used for authenticating and opening the IRMA fridge by showing relevant
IRMA credentials for access control. The application is configured to use IRMA static QRs.

## Server
The server side code can be started using

`irma-fridge server <path-to-config-dir>`

The server is used by the `irma server` to post IRMA JWTs to of disclose sessions started
by a static QR. This server checks this JWT and if correct, sends a message to the
`irma-fridge client` that the fridge can be opened via a persistent websocket connection.

An example of the configuration directory is added in this repository, called `serverconfig`.
This directory should at least contain a `config.json` file based on the format of the
`config.example.json`. All file paths in the `config.json` should be relative to the path
of the configuration directory itself.

The server configuration directory also contains examples for the `irma server` configuration
to configure a static QR for the fridge in `irmaserver-config.example.json`. The exact outline
of the QR when using the example `irma server` configuration is described in `static-qr.example.json`.

## Client

The client side of this code can be started using

`irma-fridge client <config_file>`

In the `clientconfig` directory an example configuration file can be found.
The `websocket_address` should be the URL of the `irma-fridge server` preceded by `ws://` when using
a normal websocket and `wss://` when using a secure websocket (over TLS).
Furthermore, the log file path must be relative to the directory in which the configuration file
is present and the times for `open_timeout` and `reconnect_timeout` are in milliseconds.
`open_timeout` determines how long the door is opened on one successful authentication
and `reconnect_timeout` determines the timeout on reconnecting to the websocket when the connection fails.

In the example configuration the GPIO behaviour of the fridge is disabled. When running it on the Rashberry Pi
the GPIO behaviour can be enabled by setting `enable_gpio` to `true`. Don't forget specifying the particular
GPIO pin where the relay is connected to then in `fridge_gpio_pin`.
