package main

import (
	"fmt"
	"os"
)

func main() {
	if len(os.Args) > 2 {
		switch os.Args[1] {
		case "server":
			startServer(os.Args[2])
			return
		case "client":
			startClient(os.Args[2])
			return
		}
	}
	fmt.Printf("Use \"%s server <config_dir>\" or \"%[1]s client <config_file>\" to start.\n", os.Args[0])
}
