module github.com/privacybydesign/irma-fridge

go 1.14

require (
	github.com/bwesterb/go-atum v1.0.1 // indirect
	github.com/bwesterb/go-xmssmt v1.0.4 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-errors/errors v1.0.1 // indirect
	github.com/gorilla/websocket v1.4.1
	github.com/hashicorp/go-hclog v0.10.0 // indirect
	github.com/hashicorp/go-retryablehttp v0.6.3 // indirect
	github.com/jasonlvhit/gocron v0.0.0-20191111122648-5c21418a78e8 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/privacybydesign/gabi v0.0.0-20190503104928-ce779395f4c9 // indirect
	github.com/privacybydesign/irmago v0.4.1
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/stianeikeland/go-rpio v4.2.0+incompatible
	github.com/timshannon/bolthold v0.0.0-20191009161117-ccb01ed9dec4 // indirect
	github.com/x-cray/logrus-prefixed-formatter v0.5.2 // indirect
	go.etcd.io/bbolt v1.3.3 // indirect
	golang.org/x/crypto v0.0.0-20191117063200-497ca9f6d64f // indirect
	golang.org/x/sys v0.0.0-20191118133127-cf1e2d577169 // indirect
)
