package main

import (
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/websocket"
	irma "github.com/privacybydesign/irmago"
	"github.com/privacybydesign/irmago/server"
	"io/ioutil"
	"log"
	"net/http"
	"path/filepath"
	"time"
)

type ServerConfiguration struct {
	Address            string `json:"address"`
	EnableTLS          bool   `json:"enable_tls"`
	TLSCertificateFile string `json:"tls_certificate_file"`
	TLSKeyFile         string `json:"tls_secret_key_file"`
	JWTPublicKeyFile   string `json:"jwt_public_key_file"`
	PingInterval       int    `json:"ping_interval"`
}

var (
	send          = make(chan string)
	newConnection = make(chan *websocket.Conn)
)

var serverConf ServerConfiguration

var connections []*websocket.Conn

var jwtPublicKey *rsa.PublicKey

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func handleConnect(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("Connection could not be upgraded to ws:", err)
		return
	}
	err = ws.SetReadDeadline(time.Now().Add(2 * time.Duration(serverConf.PingInterval) * time.Second))
	if err != nil {
		log.Println("ReadDeadline could not be set to ws:", err)
		return
	}
	ws.SetPongHandler(func(string) error {
		return ws.SetReadDeadline(time.Now().Add(2 * time.Duration(serverConf.PingInterval) * time.Second))
	})
	log.Println("New websocket connected:", ws.RemoteAddr().String())

	// Read connection to detect when client disconnects
	go func() {
		for {
			_, _, err = ws.NextReader()
			if err != nil {
				_ = ws.Close()
				return
			}
		}
	}()

	newConnection <- ws
}

func handleOpenFridge(w http.ResponseWriter, r *http.Request) {
	jwtBytes, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	claims := struct {
		jwt.StandardClaims
		*server.SessionResult
	}{}
	_, err = jwt.ParseWithClaims(string(jwtBytes), &claims, func(token *jwt.Token) (interface{}, error) {
		return jwtPublicKey, nil
	})
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Fixed attribute values are used in session request, so attribute values don't have to be checked here too
	if claims.ProofStatus != irma.ProofStatusValid {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	// All checks okay, fridge can be opened
	log.Println("Sending open message to fridge")
	send <- "open"
}

func handleWebsocketConnections() {
	ticker := time.NewTicker(time.Duration(serverConf.PingInterval) * time.Second)
	for {
		select {
		case <-ticker.C:
			sendToAllConnections(websocket.PingMessage, "")
		case msg := <-send:
			sendToAllConnections(websocket.TextMessage, msg)
		case conn := <-newConnection:
			connections = append(connections, conn)
		}
	}
}

func sendToAllConnections(messageType int, message string) {
	var activeConn []*websocket.Conn
	for _, conn := range connections {
		err := conn.WriteMessage(messageType, []byte(message))
		if err == nil {
			activeConn = append(activeConn, conn)
		} else {
			log.Println("Removing connection:", conn.RemoteAddr().String())
		}
	}
	connections = activeConn
}

func startServer(configDir string) {
	// Load configuration
	configFile := filepath.Join(configDir, "config.json")
	confJson, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Fatal("Configuration file could not be found, looked at location: ", configFile)
	}
	err = json.Unmarshal(confJson, &serverConf)
	if err != nil {
		log.Fatal("Configuration file could not be parsed: ", err)
	}
	serverConf.TLSKeyFile = filepath.Join(configDir, serverConf.TLSKeyFile)
	serverConf.TLSCertificateFile = filepath.Join(configDir, serverConf.TLSCertificateFile)
	serverConf.JWTPublicKeyFile = filepath.Join(configDir, serverConf.JWTPublicKeyFile)

	// Load JWT key
	bts, err := ioutil.ReadFile(serverConf.JWTPublicKeyFile)
	if err != nil {
		log.Fatal("JWT public key could not be found: ", err)
	}
	jwtPublicKey, err = jwt.ParseRSAPublicKeyFromPEM(bts)
	if err != nil {
		log.Fatal("JWT public key could not be parsed: ", err)
	}

	// Thread that deals with all websocket connection handling
	go handleWebsocketConnections()

	http.HandleFunc("/connect", handleConnect)
	http.HandleFunc("/openfridge/", handleOpenFridge)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, _ = fmt.Fprintf(w, "Hi, this is the irma-fridge server.")
	})

	log.Println("irma-fridge server is running at", serverConf.Address)

	if serverConf.EnableTLS {
		log.Fatal(http.ListenAndServeTLS(serverConf.Address, serverConf.TLSCertificateFile, serverConf.TLSKeyFile, nil))
	} else {
		log.Fatal(http.ListenAndServe(serverConf.Address, nil))
	}
}
