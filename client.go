package main

import (
	"log"
	"github.com/gorilla/websocket"
	"time"
	"sync"
	"io/ioutil"
	"encoding/json"
	"os"
	"path/filepath"
	"net"
	"net/url"

	"github.com/stianeikeland/go-rpio"
)

type ClientConfiguration struct {
	WebsocketAddress string `json:"websocket_address"`
	EnableGPIO bool `json:"enable_gpio"`
	FridgeGPIOPin int `json:"fridge_gpio_pin"`
	LogFile string `json:"log_file"`
	OpenTimeout int `json:"open_timeout"`
	ReconnectTimeout int `json:"reconnect_timeout"`
	PingInterval int `json:"ping_interval"`
}

var (
	clientConfig ClientConfiguration
	openFridgePin rpio.Pin
	mutex sync.Mutex
	activeOpenings int = 0
)

func openFridge() {
	log.Println("Opening fridge door")

	mutex.Lock()
	if activeOpenings == 0 && clientConfig.EnableGPIO {
		openFridgePin.Low()
	}
	activeOpenings++
	mutex.Unlock()

	time.Sleep(time.Duration(clientConfig.OpenTimeout) * time.Second)

	mutex.Lock()
	activeOpenings--
	if activeOpenings == 0 && clientConfig.EnableGPIO {
		openFridgePin.High()
	}
	mutex.Unlock()
}

func startClient(configFile string) {
	confJson, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Fatal("Configuration file could not be found at specified location: ", configFile)
	}
	err = json.Unmarshal(confJson, &clientConfig)

	// Init log file
	if clientConfig.LogFile != "" {
		log.Println("Logs printed to logfile", clientConfig.LogFile)
		f, err := os.OpenFile(filepath.Join(filepath.Dir(configFile), clientConfig.LogFile), os.O_RDWR|os.O_APPEND|os.O_CREATE, 0666)
		if err != nil {
			log.Fatal("Log file cannot be opened: ", err)
		}
		defer f.Close()
		log.SetOutput(f)
	}

	// Init Raspberry Pi lib if enabled
	if clientConfig.EnableGPIO {
		err := rpio.Open()
		if err != nil {
			log.Fatal("Unable to open gpio: ", err)
		}
		defer rpio.Close()
		openFridgePin = rpio.Pin(clientConfig.FridgeGPIOPin)
		openFridgePin.Output()
		openFridgePin.High()
	}

	websocketURL := clientConfig.WebsocketAddress+"/connect"
	parsedWebsocketURL, err := url.Parse(websocketURL)
	if err != nil {
		log.Fatal("WebsocketAddress URL cannot be parsed:", err)
	}

	// Continuously check IP address websocket url to detect changes
	var currentIP string
	go func() {
		for _ = range time.Tick(time.Duration(clientConfig.PingInterval) * time.Second) {
			resolvedAddr, err := net.ResolveIPAddr("ip", parsedWebsocketURL.Hostname())
			if err != nil {
				log.Println("Problem detected with DNS:", err)
				continue
			}
			currentIP = resolvedAddr.String()
		}
	}()

	log.Println("Connecting with irma-fridge server on", websocketURL)
	for {
		conn, rsp, err := websocket.DefaultDialer.Dial(websocketURL, nil)

		// If connection cannot be established, sleep and try again later
		if err != nil || rsp.StatusCode >= 400 {
			log.Println(err)
			log.Printf("Connection lost with irma-fridge server, retrying in %d seconds\n", clientConfig.ReconnectTimeout)
			time.Sleep(time.Duration(clientConfig.ReconnectTimeout) * time.Second)
			continue
		}

		connectedIP, _, err := net.SplitHostPort(conn.RemoteAddr().String())
		// If error is returned, remote address did not contain a port number
		if err != nil {
			connectedIP = conn.RemoteAddr().String()
		}
		currentIP = connectedIP
		log.Println("Connected with irma-fridge at", connectedIP)

		// Set handler for checking whether connection is still active
		conn.SetReadDeadline(time.Now().Add(2 * time.Duration(clientConfig.PingInterval) * time.Second))
		defaultPingHandler := conn.PingHandler()
		conn.SetPingHandler(func(msg string) error {
			// Only handle ping message if IP address is still up-to-date
			if (currentIP != connectedIP) {
				log.Println("IP change detected, new ip is", currentIP)
				_ = conn.Close()
				return nil
			}
			err := conn.SetReadDeadline(time.Now().Add(2 * time.Duration(clientConfig.PingInterval) * time.Second))
			if err != nil {
				return err
			}
			return defaultPingHandler(msg)
		})

		// Check for server messages
		for {
			_, bts, err := conn.ReadMessage()
			if err != nil {
				// Problems with the connection, restart connection
				break
			}
			msg := string(bts)
			if msg == "open" {
				go openFridge()
			} else {
				log.Println("Received unknown message:", msg)
			}
		}
		_ = conn.Close()
	}
}
